export class Produto {

    id : number;
    nome: string;
    setor: string;
    quantidade: string;
    valor: string;
    

    constructor(id?: number, nome?: string, setor?: string, quantidade?: string, valor?: string) {
        this.id = id;
        this.nome = nome;
        this.setor = setor;
        this.quantidade = quantidade;
        this.valor = valor;
        
    }


}