import { Component, OnInit } from '@angular/core';
import { Produto } from '../../produto';
import { ProdutoService } from '../produto.service';

@Component({
  selector: 'app-cadastro-produto',
  templateUrl: './cadastro-produto.component.html',
  styleUrls: ['./cadastro-produto.component.css']
})
export class CadastroProdutoComponent implements OnInit {

  newProduto: Produto;
  produtos: Produto[];
  showMessageError: boolean;
  
  
  constructor(private produtoService: ProdutoService) { }

  ngOnInit() {
    this.newProduto = new Produto();
    this.produtos = this.produtoService.getProdutos();
  }

  editProduto() {
    this.produtoService.updateProduto(this.newProduto);
  }

  saveProduto() {
    if (!this.newProduto.nome || this.newProduto.nome.trim() == '') {
      this.showMessageError = true;
    } else {
      this.showMessageError = false;
      if (!this.newProduto.quantidade || this.newProduto.quantidade.trim() == '') {
        this.showMessageError = true;
      } else {
        this.showMessageError = false;
        if (!this.newProduto.valor || this.newProduto.valor.trim() == '') {
          this.showMessageError = true;
        } else {
          this.showMessageError = false;
          if (!this.newProduto.setor || this.newProduto.setor.trim() == '') {
            this.showMessageError = true;
          } else {
            this.showMessageError = false;
            if(!this.newProduto.id) {
              this.newProduto.id = (new Date().getTime());
              this.produtoService.addProduto(this.newProduto);
            } else {
              this.produtoService.updateProduto(this.newProduto);
            }
            
          }
        }
      }
    }
    this.showMessageError = false;
    this.newProduto = new Produto();
  }

  selectProduto(produto : Produto){
    //this.produtoService.updateProduto(produto);
    this.newProduto = produto;
  }

  deleteProduto(produto: Produto) {
    this.produtoService.removeProduto(produto);
  }



}
