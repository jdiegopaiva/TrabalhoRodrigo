import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClienteService } from '../cliente.service';
import { Cliente } from '../../cliente';

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.css']
})
export class CadastroClienteComponent implements OnInit {

  newCliente: Cliente;
  clientes: Cliente[];
  showMessageError: boolean;
  nextId: string;

  constructor(private clienteService: ClienteService) {
  }

  ngOnInit() {
    this.newCliente = new Cliente();
    this.showMessageError = false;
    this.clientes = this.clienteService.getClientes();
  }

  loadClientes() {
    this.clientes = JSON.parse(localStorage.getItem('clientes' ));
  }

  saveCliente() {
    if (!this.newCliente.nome || this.newCliente.nome.trim() == '') {
      this.showMessageError = true;
    } else {
      this.showMessageError = false;
      if (!this.newCliente.sobrenome || this.newCliente.sobrenome.trim() == '') {
        this.showMessageError = true;
      } else {
        this.showMessageError = false;
        if (!this.newCliente.cpf || this.newCliente.cpf.trim() == '') {
          this.showMessageError = true;
        } else {
              this.showMessageError = false;
              if (!this.newCliente.email || this.newCliente.email.trim() == '') {
                this.showMessageError = true;
              } else {
                this.showMessageError = false;
                if (!this.newCliente.telefone || this.newCliente.telefone.trim() == '') {
                  this.showMessageError = true;
                } else {
                  this.showMessageError = false;
                  if (!this.newCliente.id) {
                    this.newCliente.id = (new Date().getTime());
                    this.clienteService.addCliente(this.newCliente);
                  } else{
                    this.clienteService.updateCliente(this.newCliente);
                  }
                }
              }
            
          
        }
      }
    }
    this.showMessageError = false;
    this.newCliente = new Cliente();
  }

  deleteCliente(cliente: Cliente) {
    this.clienteService.removeCliente(cliente);
  }

  selectCliente(cliente : Cliente) {
    this.newCliente = cliente;
  }

  editCliente(cliente : Cliente) {
    this.clienteService.updateCliente(this.newCliente);
  }

}
