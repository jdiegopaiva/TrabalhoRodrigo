import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CadastroClienteComponent } from './cadastro-cliente/cadastro-cliente.component';
import { CadastroProdutoComponent } from './cadastro-produto/cadastro-produto.component';
import { ClienteService } from './cliente.service';
import { RouteModule } from './route.module';
import { CpfPipe } from './cpf.pipe';
import { ProdutoService } from './produto.service';

@NgModule({
  declarations: [
    AppComponent,
    CadastroClienteComponent,
    CadastroProdutoComponent,
    CpfPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouteModule
    
  ],
  providers: [ClienteService, ProdutoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
