import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CadastroClienteComponent } from './cadastro-cliente/cadastro-cliente.component';
import { CadastroProdutoComponent } from './cadastro-produto/cadastro-produto.component';

const routes : Routes = [
  { path : 'cadastro-cliente', component : CadastroClienteComponent },
  { path : 'cadastro-produto', component : CadastroProdutoComponent }
];

@NgModule({
  exports : [
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class RouteModule { }
