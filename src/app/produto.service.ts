import { Injectable } from '@angular/core';
import { Produto } from '../produto';

@Injectable()
export class ProdutoService {

  produtos: Produto[];

  constructor() { 
    this.produtos = [];
  }

  getProdutos(): Produto[] {
    if (localStorage.getItem('produtos') === null) {
      this.produtos = [];
    } else {
      this.produtos = JSON.parse(localStorage.getItem('produtos'));
    }
    return this.produtos;
  }

  addProduto(produto: Produto): void {
    let dados;
    this.produtos.push(produto);
    localStorage.setItem('produtos', JSON.stringify(this.produtos));
  }

  removeProduto(produto : Produto){
    let index = this.produtos.indexOf(produto);
    this.produtos.splice(index, 1);
    localStorage.setItem('produtos', JSON.stringify(this.produtos));
  }

//  updateProduto(produto : Produto){
//    let index = this.produtos.indexOf(produto);
//    this.produtos[index] = produto;
 //   localStorage.setItem('produtos', JSON.stringify(this.produtos))
//
  //}

  updateProduto(produto : Produto){
    let existProduto = this.produtos.find(prod => prod.id == produto.id);
    existProduto.nome = produto.nome;
    existProduto.quantidade = produto.quantidade;
    existProduto.valor = produto.valor;
    existProduto.setor = produto.setor;
    localStorage.setItem('produtos', JSON.stringify(this.produtos));

  }


}
