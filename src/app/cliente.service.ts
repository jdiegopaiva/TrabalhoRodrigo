import { Injectable } from '@angular/core';
import { Cliente } from '../cliente';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class ClienteService {

  clientes: Cliente[];
  private nextId: number;

  constructor() {
    this.clientes = [];

  }

  getClientes(): Cliente[] {
    if (localStorage.getItem('clientes') === null) {
      this.clientes = [];
    } else {
      this.clientes = JSON.parse(localStorage.getItem('clientes'));
    }
    return this.clientes;
  }

  addCliente(cliente: Cliente): void {
    let dados;
    this.clientes.push(cliente);
    localStorage.setItem("clientes",JSON.stringify(this.clientes) );

  }

  removeCliente(cliente: Cliente) {
    let index = this.clientes.indexOf(cliente);
    this.clientes.splice(index, 1);
    localStorage.setItem('clientes', JSON.stringify(this.clientes));
  }

  setLocalStorage(clientes: Cliente[]) {
    localStorage.setItem('clientes', JSON.stringify({ clientes: clientes }));
  }

  updateCliente(cliente : Cliente) {
    let existCliente = this.clientes.find(cli => cli.id == cliente.id);
    existCliente.nome = cliente.nome;
    existCliente.sobrenome = cliente.sobrenome;
    existCliente.cpf = cliente.cpf;
    existCliente.email = cliente.email;
    existCliente.telefone = cliente.telefone;
    localStorage.setItem('clientes', JSON.stringify(this.clientes));
   }

}
