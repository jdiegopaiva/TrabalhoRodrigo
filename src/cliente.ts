export class Cliente {

    id : number;
    nome: string;
    sobrenome: string;
    cpf: string;
    email: string;
    telefone: string;

    constructor(id?: number, nome?: string, sobrenome?: string, cpf?: string, email?: string, telefone?: string) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.cpf = cpf;
        this.email = email;
        this.telefone = telefone;
    }


}